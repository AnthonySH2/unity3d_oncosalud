﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class InitializationVR : MonoBehaviour {

    

    // Use this for initialization
    void Start() {
        StartCoroutine(LoadDevice("CardBoard", true));
    }

    public IEnumerator LoadDevice(string nameDevice, bool active)
    {
        VRSettings.LoadDeviceByName(nameDevice);
        yield return null;
        VRSettings.enabled = active;
    }

    
}
