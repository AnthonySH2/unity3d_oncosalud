﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VRStandardAssets.Utils;
using DG.Tweening;

public class InteractiveButtonInstrucciones : MonoBehaviour {


    public event Action<InteractiveButtonInstrucciones> OnButtonSelected;                   // This event is triggered when the selection of the button has finished.

    public GameObject _DesactivateObject;
    public GameObject _ActivateObject;

                       // The name of the scene to load.
    [SerializeField] private VRCameraFade m_CameraFade;                 // This fades the scene out when a new scene is about to be loaded.
    [SerializeField] private SelectionRadial m_SelectionRadial;         // This controls when the selection is complete.
    [SerializeField] private VRInteractiveItem m_InteractiveItem;       // The interactive item for where the user should click to load the level.


    private bool m_GazeOver;                                            // Whether the user is looking at the VRInteractiveItem currently.


    private void OnEnable()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_SelectionRadial.OnSelectionComplete += HandleSelectionComplete;
    }


    private void OnDisable()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_SelectionRadial.OnSelectionComplete -= HandleSelectionComplete;
    }


    private void HandleOver()
    {
        // When the user looks at the rendering of the scene, show the radial.
        m_SelectionRadial.Show();

        m_GazeOver = true;
    }


    private void HandleOut()
    {
        // When the user looks away from the rendering of the scene, hide the radial.
        m_SelectionRadial.Hide();

        m_GazeOver = false;
    }


    private void HandleSelectionComplete()
    {
        // If the user is looking at the rendering of the scene when the radial's selection finishes, activate the button.
        if (m_GazeOver)
            StartCoroutine(ActivateButton());
    }


    private IEnumerator ActivateButton()
    {
        // If the camera is already fading, ignore.
        if (m_CameraFade.IsFading)
            yield break;

        // If anything is subscribed to the OnButtonSelected event, call it.
        if (OnButtonSelected != null)
            OnButtonSelected(this);
       
        // Wait for the camera to fade out.
        
        Reiniciar();
        // Accion al presionar el button

    }


    void Reiniciar()
    {
        Debug.Log("Metodo Reiniciar Activadoo huachin....!!!");
        _DesactivateObject.transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
         _ActivateObject.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBounce)
        );
        
    }
}
