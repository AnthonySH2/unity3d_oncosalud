﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Dialog360 : MonoBehaviour {
    public GameObject CanvasText;
    public List<Comment> guias;
    // Use this for initialization
    bool GuiasEnabled = true;
    //bool instanciado = false;    
    GameObject guia;
    int index = 0;

    void Start () {
        index = 0;
        Debug.Log(guias[2].timeToApear);
	}
	
	// Update is called once per frame
	void Update () {
        
        if (GuiasEnabled) {
           float timePlaying=(float)FindObjectOfType<VideoPlayer>().time;            
            if (Mathf.Abs(timePlaying - guias[index].timeToApear) <= 0.05f)
            {
                //Rango de Instanciamiento
                
                if (!guias[index].instanciado)
                {
                    if (!guias[index].isChildOfCamera)                    
                        guia = Instantiate(CanvasText, guias[index].posicion.position, guias[index].posicion.rotation, guias[index].posicion);
                    else
                        guia = Instantiate(CanvasText, Camera.main.transform.position + Camera.main.transform.forward*3f+ 
                            Vector3.right*guias[index].OffSet.x + Vector3.up * guias[index].OffSet.y, Camera.main.transform.rotation, Camera.main.transform);
                   
                    guia.transform.Find("Text").GetComponent<Text>().text = guias[index].textToplace;
                    guia.transform.Find("Text").GetComponent<Text>().color = guias[index].colorText;
                    if (guias[index].textToplace == "Girar a tu izquierda o derecha") {
                        guia.transform.Find("LeftArrow").GetComponent<Image>().enabled = true;
                        guia.transform.Find("RightArrow").GetComponent<Image>().enabled = true;
                    }

                    if (guias[index].textToplace == "Girar a tu izquierda")
                        guia.transform.Find("LeftArrow").GetComponent<Image>().enabled = true;

                    Destroy(guia.gameObject, guias[index].timeToDestroy);
                    guias[index].instanciado = true;
                    index++;

                    Debug.Log(guias.Count + "==" + index);
                    if (index == guias.Count )
                        index = -1;

                    Debug.Log(guias.Count + "==" + index);


                    
                   
                }
            }
            /*
            else {
                instanciado = false;
            } */          
         }
		
	}
    [System.Serializable]
    public class Comment {
        public float timeToApear;
        public float timeToDestroy;
        public string textToplace;
        public Color colorText;
        public bool isChildOfCamera;
        public Transform posicion;
        public bool instanciado;
        public Vector2 OffSet;
    }

    public void setPanels() {
        GuiasEnabled = true;
    }
    public void stopPanels()
    {
        foreach (var item in guias)
        {
            item.instanciado = false;
        }
        index = 0;
        GuiasEnabled = false;
    }
}

